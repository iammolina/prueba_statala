import React, { Component } from 'react';
import Form from 'react-formal';
import { Redirect } from 'react-router';
import { modelSchema } from './schemas/user';
import { Auth } from './components/api';
import history from './history';


export default class Login extends Component {

    constructor(){
        super();
        this.state = 
        {
            model: modelSchema,
            errorLogin: false,
            redirect : false
        }
    }

    onSubmit(formValues) 
    {
       
        Auth(formValues.user)
            .then((response) => 
            {
                if (response.data.msg === "success") 
                {
                    localStorage.setItem('user', JSON.stringify(response.data.data));

               

                    if(response.data.data[0].id_tipouser === 1)
                    {
                      history.replace('/admin');
                    }else{
                      history.replace('/user');
                    }
                   
                }

            })
            .catch((error) => {
                this.setState({
                    errorLogin: true
                })
            })

    }

    render() 
    {
        const { errorLogin, model , redirect } = this.state;

        if (redirect) 
        {
            return <Redirect to='/' />;
        }

        return (
            

            <div className="container">

             <div className="login">


                        <h2 className="text-center"> Login </h2>


                        {errorLogin === true &&
                            <div className="alert alert-danger">
                                Datos de login incorrectos
                                </div>
                        }


                        <Form
                            onSubmit={(e) => this.onSubmit(e)}
                            schema={modelSchema}
                            value={model}
                            onChange={modelInfo => this.setState({ model: modelInfo })}
                        >

                            <div style={{ "marginTop": "10px" }} className="form-group">
                                <label>Mail</label>
                            <Form.Field className={`form-control col-md-12`} name='user.mail' placeholder='email@prueba.cl' />
                            <Form.Message errorClass={`col-md-12 alert alert-danger`} for='user.mail' />
                            </div>

                            <br />

                            <div className="form-group">
                                <label>Clave</label>
                            <Form.Field type="password" className={`form-control col-md-12`} name='user.password' placeholder='***********' />
                                <Form.Message errorClass={`col-md-12 alert alert-danger`} for='user.password' />
                            </div>

                            <br />

                            <Form.Button style={{ "marginTop": "10px" }} className={`btn btn-block btn-info`} type='submit'>
                                Login
                            </Form.Button>

                            <a className=" btn btn-primary btn-block text-center" href="registro"> Registrar </a>
                        </Form>
                </div>


            </div>
        )
    }
}