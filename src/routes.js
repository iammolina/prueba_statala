import React from 'react';
import { Route, Router } from 'react-router-dom';
import App  from './App';
import Admin from './components/admin';
import User from './components/user';
import Registro from './components/registro';

import history from './history';

export const makeMainRoutes = () => 
{
  return (
      <Router history={history}>
        <div>
          <Route path="/" exact component={App}/>
          <Route path="/admin" component={Admin} />
          <Route path="/user" component={User} />
          <Route path="/registro" component={Registro} />
          
        </div>
      </Router>
  );
}
