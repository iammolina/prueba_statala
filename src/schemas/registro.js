import * as yup from 'yup';

export const modelSchemaRegistro = yup.object({
    user: yup.object({
        nombre: yup.string()
        .required('El nombre es requerido'),
        tipo_iduser: yup.string()
        .required('El tipo usuario es requerido'),
        mail: yup.string()
            .email('El formato del email no es correcto')
            .required('El email es requerido'),
        password: yup.string()
            .required('El password es requerido')
            .min(6, "El password no puede tener menos de 6 caracteres")
    })
})