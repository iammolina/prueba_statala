import * as yup from 'yup';

export const modelSchema = yup.object({
    user: yup.object({
        mail: yup.string()
            .email('El formato del email no es correcto')
            .required('El email es requerido'),
        password: yup.string()
            .required('El password es requerido')
            .min(6, "El password no puede tener menos de 6 caracteres")
    })
})