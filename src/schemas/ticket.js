import * as yup from 'yup';

export const modelSchemaTicket = yup.object({
    tck: yup.object({
        id_user: yup.string()
            .required('Usuario es requerido'),
        ticket_pedido: yup.string()
            .required('El pedido es requerido')
            .min(6, "El pedido no puede tener menos de 6 caracteres")
    })
})