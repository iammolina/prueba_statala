import axios from 'axios';
axios.defaults.baseURL = process.env.REACT_APP_API_URL;
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';


//LOGIN

export function Auth(user) 
{
    return axios.post('users', user)
}


export function postRegistro(user) 
{
    return axios.post('users/registro', user)
}


export function getUsers() 
{
    return axios.get('users')
}

//TICKET
export function getTicket() 
{
    return axios.get('ticket');
}

export function getTicketUser(id) 
{
    return axios.get('ticket/' + id);
}



export function postTicket(data) 
{
    return axios.post('ticket' , data);
}



export function putTicket(data) 
{
    return axios.put('ticket' , data);
}


export function deleteTicket(id) 
{
    return axios.delete('ticket/' +  id);
}

