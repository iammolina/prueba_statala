import Form from 'react-formal';
import React, { Component } from 'react';
import { postTicket , getUsers } from "./api";
import { modelSchemaTicket } from '../schemas/ticket'
import { Alert } from 'react-bootstrap';

export default class AddTicket extends Component 
{

    constructor(props) {
        super();
        
        this.state = {
            model: modelSchemaTicket,
            isLoading : false,
            error: false,
            users : [],
            updated :false
        }

        this.onSubmit = this.onSubmit.bind(this);
    }

    componentDidMount()
    {

        getUsers()
        .then((response) =>
        {
            this.setState({ users : response.data });

        }).catch((error) => {
            alert('error');
        });



    }

    onSubmit(e) 
    {

        var obj = {
            id_user : e.tck.id_user,
            descripcion : e.tck.ticket_pedido
        }

        postTicket(obj)
            .then((response) =>
            {
                this.setState({ updated : true });

                setTimeout(() => 
                {
                    this.setState({ updated: false });
                }, 3000);

            }).catch((error) => {
                alert('error');
            });

    }




    render() {

      
        const { users , updated } = this.state;


      
     

       
        return (
            <div>

            {updated === true &&
                <Alert bsStyle="success" >
                    El ticket se agrego correctamente
                </Alert>
                
            }

              
                <Form
                    onSubmit={(e) => this.onSubmit(e)}
                    onChange={modelInfo => this.setState({ model: modelInfo })}
                    schema={modelSchemaTicket}
                >


                    <div style={{ "marginTop": "10px" }} className="form-group">
                        <label>Usuario</label>
                        <Form.Field className={`form-control col-md-12`} name='tck.id_user' type='select'>
                            <option value={null}>Seleccionar área o unidad de negocio...</option>

                                {users.map(function(u){
                                    return <option value={u.id}>{u.nombre}</option>
                                })}

                          

                        </Form.Field>

                        <Form.Message errorClass={`col-md-12 alert alert-danger`} for='tck.id_user' />
                    </div>

                
                    <div style={{ "marginTop": "10px" }} className="form-group">
                        <label>Descripcion</label>
                        <Form.Field type="textarea" className={`form-control col-md-12`} name='tck.ticket_pedido' placeholder='Descripcion' />
                        <Form.Message errorClass={`col-md-12 alert alert-danger`} for='tck.ticket_pedido' />
                    </div>


                   

                    <div className="clearfix"></div>

                    <Form.Button style={{ "marginTop": "10px" }} className={`btn btn-block btn-success`} type='submit'>Agregar</Form.Button>

                </Form>
            </div>
        )


    }
}