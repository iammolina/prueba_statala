import React, { Component } from 'react';
import { Navbar , NavItem , Nav} from 'react-bootstrap';
import history from '../history';

class NavApp extends Component 
{
    constructor(props) 
    {
        super();

        this.state = {
            user: JSON.parse(localStorage.getItem('user')),
        }
      
        this.logout = this.logout.bind(this);

    }

    componentDidMount(){

        if(!localStorage.user)
        {
            history.replace('/');
        }

    }


    logout()
    {
        localStorage.clear();
        history.replace('/');
    }



    render() {

        const { user } = this.state;

        if(!user)
        {
            return null;
        }
      
        return (
            <Navbar inverse collapseOnSelect>
                <Navbar.Header>
                    <Navbar.Brand>
                    <a href="#brand">Prueba</a>
                    </Navbar.Brand>
                    <Navbar.Toggle />
                </Navbar.Header>
                <Navbar.Collapse>
                   
                    <Nav pullRight>
                    <NavItem eventKey={1} href="#">
                        {user[0].mail}
                    </NavItem>
                    <NavItem eventKey={2} onClick={this.logout}>
                        cerrar sesion
                    </NavItem>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
            
        );
    }
}

export default NavApp;