import React, { Component } from 'react';
import { getTicketUser } from './api';
import { Table } from 'react-bootstrap';
import NavApp from './nav';
import history from '../history';

class ListTable extends React.Component 
{
  render()
  {
      const {tick } = this.props;

      return (
          <tr>
              <td>{tick.id}</td>
              <td>{tick.ticket_pedido}</td>
          </tr> 
         
      );
  }
}



class Admin extends Component 
{

    constructor(props){

        super(props);

        this.state = {
            tickets : [],
            user: JSON.parse(localStorage.getItem('user')),
        }
       
    }


    componentWillMount()
    {
        if(localStorage.user)
        { 

            const user = this.state.user;
            getTicketUser(user[0].id)
            .then((response) => 
            {
               this.setState({tickets : response.data});
              
            }).catch((error) => 
            {
               alert('error');
            });
        }else{
            history.replace('/');
        }
    }

  
    

    render() 
    {

        const {tickets} = this.state;


        if(tickets.length === 0)
        {
            return null;
        }


        const createProyecto = (ticket , index) => 
        {
        
            return <ListTable key={index} tick={ticket} onDelete={() => this._delete(ticket.id)} />
        }

        return (

            <div>

            <NavApp/>

            <div className="container">

                <Table striped bordered condensed hover>
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>ticket</th>
                        </tr>
                    </thead>
                    <tbody>

                     { tickets.length > 0 && tickets.map(createProyecto) }
                    

                    </tbody>
                </Table>

               

            </div>

            </div>
           
        );
    }
}

export default Admin;