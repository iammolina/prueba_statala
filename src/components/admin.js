import React, { Component } from 'react';
import { getTicket , deleteTicket} from './api';
import { Table , Button , Modal  } from 'react-bootstrap';
import Ticket from './ticket';
import AddTicket from './add_ticket';
import NavApp from './nav';

class ListTable extends React.Component 
{
  render()
  {
      const {tick , onDelete , onUpdate} = this.props;

      return (
          <tr>
              <td>{tick.id}</td>
              <td>{tick.id_user}</td>
              <td>{tick.ticket_pedido}</td>
              <td>{ <Button bsStyle="info" bsSize="xsmall" onClick={onUpdate}>Editar</Button> }</td>
              <td>{ <Button bsStyle="danger" bsSize="xsmall" onClick={onDelete}>Eliminar</Button> }</td>
          </tr> 
         
      );
  }
}



class Admin extends Component 
{

    constructor(props){

        super(props);

        this.state = {
            tickets : [],
            ticket : null,
            idTicket : 0,
            show : false,
            show_eliminar : false,
            show_add : false,
            user: JSON.parse(localStorage.getItem('user')),
        }

        this._delete      = this._delete.bind(this);
        this.handleClose   = this.handleClose.bind(this);
        this.handleDelete  = this.handleDelete.bind(this);
        this.handleUpdate  = this.handleUpdate.bind(this);
        this.handleAdd     = this.handleAdd.bind(this);
    }


    componentDidMount()
    {

        getTicket()
        .then((response) => 
        {
           this.setState({tickets : response.data});
          
        }).catch((error) => 
        {
           alert('error');
        });
    }

  

    _delete(id)
    {
         this.setState({  idTicket  : id , show_eliminar : true  });
    }

    handleAdd()
    {
        this.setState({  show_add : true  }); 
        
    }

    handleDelete(){

        const id = this.state.idTicket;

        deleteTicket(id)
        .then((response) => 
        {
            
            this.componentDidMount();
            this.setState({ show_eliminar : false  });
          
        }).catch((error) => 
        {
           alert('error');
        });

        
    }

    handleUpdate(tick)
    {

        this.setState({ show : true , ticket : tick  });
        

    }

    handleClose()
    {
        this.setState({ show: false , show_eliminar : false  , show_add :false });
        this.componentDidMount();

    }
    

    render() 
    {

        const {tickets ,  idTicket , ticket} = this.state;


        if(tickets.length === 0)
        {
            return null;
        }


        const createProyecto = (ticket , index) => 
        {
        
            return <ListTable key={index} tick={ticket} onUpdate={() => this.handleUpdate(ticket)}   onDelete={() => this._delete(ticket.id)} />
        }

        return (

            <div>
                   <NavApp />
           

            <div className="container">
               

                <Button bsStyle="primary" bsSize="xsmall" onClick={() => this.handleAdd() } >Agregar</Button>

                <Table striped bordered condensed hover>
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>usuario</th>
                            <th>ticket</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                     { tickets.length > 0 && tickets.map(createProyecto) }
                    

                    </tbody>
                </Table>


                

                <Modal show={this.state.show_add} onHide={this.handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Add Ticket</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>

                         <AddTicket/>
                    

                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.handleClose}>Close</Button>
                    </Modal.Footer>
                </Modal>


                <Modal show={this.state.show} onHide={this.handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Update Ticket</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>

                         <Ticket  tck={ ticket } />
                    

                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.handleClose}>Close</Button>
                    </Modal.Footer>
                </Modal>

                <Modal show={this.state.show_eliminar} onHide={this.handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Eliminar</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>

                        <h4 className="text-center">Seguro de eliminar el ticket { idTicket } ?</h4>
                    

                    </Modal.Body>
                    <Modal.Footer>
                        <Button bsStyle="danger" onClick={this.handleDelete}>Eliminar</Button>
                        <Button onClick={this.handleClose}>Close</Button>
                    </Modal.Footer>
                </Modal>

            </div>

            </div>
           
        );
    }
}

export default Admin;